﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BizXpERP.DbAccess;
using BizXpERP.Models;

namespace BizXpERP.BusinessLayer
{
    public class DashboardRepository
    {
        private readonly AccessDashboard ad;
        
        public DashboardRepository(string conStr)
        {
            ad = new AccessDashboard(conStr);
        }
        public Dashboard GetDashboard(int shopid, DateTime startDate, DateTime endDate)
        {
                Dashboard dashboard = ad.GetDasboard(shopid, startDate, endDate);
                return dashboard;
        }
    }
}
