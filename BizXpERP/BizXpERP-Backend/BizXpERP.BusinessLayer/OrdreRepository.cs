﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BizXpERP.DbAccess;
using BizXpERP.Models;

namespace BizXpERP.BusinessLayer
{
    public class OrderRepository
    {
        private readonly AccessOrder ao;
        
        public OrderRepository(string conStr)
        {
            ao = new AccessOrder(conStr);
        }

        public bool AddOrder(Order order)
        {
            try
            {
                ao.AddOrder(order);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool UpdateOrder(Order order)
        {
            try
            {
                ao.UpdateOrder(order);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public Order GetOrdersById(int? id)
        {
                Order order = ao.GetOrdersById(id);
                return order;
            
        }

        public List<Order> GetOrdersByPaymentId(int? paymentId)
        {
            List<Order> orders = ao.GetOrdersByPaymentId(paymentId);
            return orders;

        }
    }
}
