﻿using BizXpERP.DbAccess;
using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.BusinessLayer
{
    public class PaymentRepository
    {
        private readonly AccessPayment ap;

        public PaymentRepository(string conStr)
        {
            ap = new AccessPayment(conStr);
        }

        public bool AddPayment(Payment payment)
        {
            try
            {
                ap.AddPayment(payment);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdatePayment(Payment payment)
        {
            try
            {
                ap.UpdatePayment(payment);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Payment GetPaymentsById(int? id)
        {
            Payment payment = ap.GetPaymentsById(id);
            return payment;

        }

        public IEnumerable<Payment> GetPaymentsByDueAmount(double dueAmount)
        {
            var payment = ap.GetPaymentsByDueAmount(dueAmount);
            return payment;
        }


        public IEnumerable<Payment> GetPaymentsByDate(DateTime startDate, DateTime endDate, int shopId)
        {
            var payment = ap.GetPaymentsByDate(startDate,endDate,shopId);
            return payment;

        }
    }
}
