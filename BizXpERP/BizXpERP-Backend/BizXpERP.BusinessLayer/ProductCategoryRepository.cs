﻿using BizXpERP.DbAccess;
using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.BusinessLayer
{
    public class ProductCategoryRepository
    {
        private readonly AccessProductCategory apc;

        public ProductCategoryRepository(string conStr)
        {
            apc = new AccessProductCategory(conStr);
        }

        public bool AddProductCategory(ProductCategory productCategory)
        {
            try
            {
                apc.AddProductCategory(productCategory);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateProductCategory(ProductCategory productCategory)
        {
            try
            {
                apc.UpdateProductCategory(productCategory);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
