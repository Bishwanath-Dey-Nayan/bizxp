﻿using BizXpERP.DbAccess;
using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.BusinessLayer
{
    public class ProductPurchaseRepository
    {
        private readonly AccessProductPurchase app;

        public ProductPurchaseRepository(string conStr)
        {
            app = new AccessProductPurchase(conStr);
        }

        public IEnumerable<ProductPurchase> GetProductPurchaseByUserIdandShopId(int userId,int shopId)
        {
            var productPurchase = app.GetProductPurchaseByUserIdandShopId(userId, shopId);
            return productPurchase;
        }

        public bool AddProductPurchase(ProductPurchase pp)
        {
            try
            {
                app.AddProuductPurchase(pp);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateProductPurchase(ProductPurchase pp)
        {
            try
            {
                app.UpdateProductPurchase(pp);
                return true;
            }

            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
