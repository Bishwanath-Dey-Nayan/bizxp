﻿using System;
using System.Collections.Generic;
using System.Text;
using BizXpERP.DbAccess;
using BizXpERP.Models;

namespace BizXpERP.BusinessLayer
{
    public class ProductRepository
    {
        private readonly AccessProduct ap;
        public ProductRepository(string conStr)
        {
            ap = new AccessProduct(conStr);
        }

        public bool AddProduct(Product product)
        {
            try
            {
                ap.AddProduct(product);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool UpdateProduct(Product product)
        {
            try
            {
                ap.UpdateProduct(product);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public Product GetProductById(int? id)
        {
            Product product = ap.GetProductsById(id);
            return product;
            
        }

        public IEnumerable<Product> GetProductsByCategoryId(int categoryId)
        {
            var product = ap.GetProductsByCategoryId(categoryId);
            return product;
        }

        public IEnumerable<Product> GetProductsByV_S_Id(int vendorId, int shopId)
        {
            var product = ap.GetProductsByV_S_Id(vendorId,shopId);
            return product;
        }

        public IEnumerable<Product> GetProductsByDate_Count(DateTime StartDate, DateTime EndDate, int count)
        {
            var product = ap.GetProductsByDate_Count(StartDate,EndDate,count);
            return product;
        }

        public IEnumerable<Product> GetProductsFromShop(int? userId, int? shopId)
        {
            var products = ap.GetProductsFromShop(userId, shopId);
            return products;
        }
    }
}
