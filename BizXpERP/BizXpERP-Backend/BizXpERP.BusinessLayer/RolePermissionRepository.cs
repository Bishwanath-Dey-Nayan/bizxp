﻿using BizXpERP.DbAccess;
using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.BusinessLayer
{
    public class RolePermissionRepository
    {
        private readonly AccessRolePermission arp;

        public RolePermissionRepository(string conStr)
        {
            arp = new AccessRolePermission(conStr);
        }

        public bool AddRolePermisson(RolePermission rp)
        {
            try
            {
                arp.AddRolePermission(rp);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateRolePermission(RolePermission rp)
        {
            try
            {
                arp.UpdateRolePermission(rp);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


    }
}
