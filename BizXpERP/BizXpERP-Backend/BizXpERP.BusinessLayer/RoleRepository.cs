﻿using BizXpERP.DbAccess;
using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.BusinessLayer
{
    public class RoleRepository
    {
        private readonly AccessRole ar;

        public RoleRepository(string conStr)
        {
            ar = new AccessRole(conStr);
        }

        public bool AddRole(Role role)
        {
            try
            {
                ar.AddRole(role);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateRole(Role role)
        {
            try
            {
                ar.UpdateRole(role);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
