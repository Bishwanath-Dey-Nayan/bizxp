﻿using BizXpERP.DbAccess;
using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.BusinessLayer
{
    public class ShopDetailRepository
    {
        private readonly AccessShopDetails asd;

        public ShopDetailRepository(string conStr)
        {
            asd = new AccessShopDetails(conStr);
        }

        public ShopDetails GetShopDetails(int? ownerId)
        {
            try
            {
                var retrievedShop = asd.GetShopDetails(ownerId);
                if (retrievedShop != null && retrievedShop.Shop_Id != 0)
                {
                    return retrievedShop;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool AddShopDetail(ShopDetails sd)
        {
            try
            {
                asd.AddShopDetails(sd);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        

        public bool UpdateShopDetail(ShopDetails sd)
        {
            try
            {
                asd.UpdateShopDetails(sd);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
