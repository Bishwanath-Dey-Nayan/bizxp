﻿using BizXpERP.DbAccess;
using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.BusinessLayer
{
    public class UserDetailsRepository
    {
        private readonly AccessUserDetails aud;

        public UserDetailsRepository(string conStr)
        {
            aud = new AccessUserDetails(conStr);
        }
        public UserDetails ValidateLogin(UserDetails user)
        {
            try
            {
                var retrievedUser = aud.GetUser(user);
                if (retrievedUser != null && retrievedUser.User_Id != 0)
                {
                    return retrievedUser;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool AddUser(UserDetails ud)
        {
            try
            {
                aud.AddUser(ud);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateUserDetails(UserDetails ud)
        {
            try
            {
                aud.UpdateUserDetails(ud);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
