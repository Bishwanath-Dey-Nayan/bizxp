﻿using BizXpERP.DbAccess;
using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.BusinessLayer
{
    public class VendorRepository
    {
        public readonly AccessVendor av;

        public VendorRepository(string conStr)
        {
            av = new AccessVendor(conStr);
        }

        public bool AddVendor(Vendor vendor)
        {
            try
            {
                av.AddVendor(vendor);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool UpdateVendor(Vendor vendor)
        {
            try
            {
                av.UpdateVendor(vendor);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

    }
}
