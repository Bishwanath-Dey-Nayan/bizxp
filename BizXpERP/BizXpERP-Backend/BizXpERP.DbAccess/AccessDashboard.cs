﻿using BizXpERP.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BizXpERP.DbAccess
{
    public class AccessDashboard
    {
        private string connectionString;

        public AccessDashboard(string con)
        {
            connectionString = con;
        }

        /// <summary>
        /// Method for getting dashboard data
        /// </summary>
        /// <param name="shopId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public Dashboard GetDasboard(int shopId, DateTime startDate, DateTime endDate)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetTotalSellAndProfit", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Shop_Id", shopId);
                cmd.Parameters.AddWithValue("@StartDate", startDate);
                cmd.Parameters.AddWithValue("@EndDate", endDate);

                con.Open();
                Dashboard dashboard = new Dashboard();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var c = dr["TotalProfit"];
                        dashboard = new Dashboard()
                        {
                            Total_Sales = Convert.ToInt32(dr["TotalSale"]),
                            //Total_Due = dr["TotalDue"].ToString(),
                            Total_Profit = dr["TotalProfit"].ToString()
                        };
                    }
                }
                con.Close();
                return dashboard;
            }
        }
    }
}
