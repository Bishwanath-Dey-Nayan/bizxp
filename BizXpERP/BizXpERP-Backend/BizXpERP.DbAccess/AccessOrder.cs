﻿using System;
using System.Data.SqlClient;
using BizXpERP.Models;
using System.Data;
using System.Collections.Generic;

namespace BizXpERP.DbAccess
{
    public class AccessOrder
    {
        private string connectionString;
        
        public AccessOrder(string con)
        {
            connectionString = con; 
        }

        /// <summary>
        /// Method for Inserting Order in the DB
        /// </summary>
        /// <param name="order"></param>
        public void AddOrder(Order order)
        {
            using (SqlConnection con = new SqlConnection(connectionString)) {
                SqlCommand cmd = new SqlCommand("InsertUpdateOrder",con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Product_Id", order.Product_Id);
                cmd.Parameters.AddWithValue("@Quantity", order.Quantity);
                cmd.Parameters.AddWithValue("@Discount", order.Discount);
                cmd.Parameters.AddWithValue("@Total_price", order.Total_price);
                cmd.Parameters.AddWithValue("@Actual_price", order.Actual_price);
                cmd.Parameters.AddWithValue("@Order_note", order.Order_note);
                cmd.Parameters.AddWithValue("@Payment_Id", order.Payment_Id);
                cmd.Parameters.AddWithValue("@Shop_Id", order.Shop_Id);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void UpdateOrder(Order order)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateOrder",con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Order_Id", order.Order_Id);
                cmd.Parameters.AddWithValue("@Product_Id", order.Product_Id);
                cmd.Parameters.AddWithValue("@Quantity", order.Quantity);
                cmd.Parameters.AddWithValue("@Discount", order.Discount);
                cmd.Parameters.AddWithValue("@Total_price", order.Total_price);
                cmd.Parameters.AddWithValue("@Actual_price", order.Actual_price);
                cmd.Parameters.AddWithValue("@Order_note", order.Order_note);
                cmd.Parameters.AddWithValue("@Payment_Id", order.Payment_Id);
                cmd.Parameters.AddWithValue("@Shop_Id", order.Shop_Id);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public Order GetOrdersById(int? id)
        {
            Order order = new Order();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetOrdersById", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Order_Id", id);
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    order.Order_Id = Convert.ToInt32(sdr["Order_Id"]);
                    order.Product_Id = (int)sdr["Product_Id"];
                    order.Quantity = Convert.ToInt32(sdr["Quantity"]);
                    order.Discount = (int)sdr["Discount"];
                    order.Total_price = (int)sdr["Total_price"];
                    order.Actual_price = (int)sdr["Actual_price"];
                    order.User_Id = (int)sdr["User_Id"];
                    order.Created_date = (DateTime)sdr["Created_date"];
                    order.Is_delete = (bool)sdr["Is_delete"];
                    order.Order_note = sdr["Order_note"].ToString();
                    order.Payment_Id = (int)sdr["Payment_Id"];
                    order.Shop_Id = (int)sdr["Shop_Id"];
                }
            }
            return order;
        }


        /// <summary>
        /// Method for retriving Order by paymentId
        /// </summary>
        /// <param name="paymentId"></param>
        /// <returns></returns>
        public List<Order> GetOrdersByPaymentId(int? paymentId)
        {
            
            List<Order> orders = new List<Order>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetOrdersByPaymentId", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Payment_Id", paymentId);
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Order order = new Order
                    {
                        Order_Id = Convert.ToInt32(sdr["Order_Id"]),
                        Product_Id = (int)sdr["Product_Id"],
                        Quantity = Convert.ToInt32(sdr["Quantity"]),
                        Discount = (decimal)sdr["Discount"],
                        Total_price = (decimal)sdr["Total_price"],
                        Actual_price = (decimal)sdr["Actual_price"],
                        User_Id = (int)sdr["User_Id"],
                        Created_date = (DateTime)sdr["Created_date"],
                        Is_delete = (bool)sdr["Is_delete"],
                        Order_note = sdr["Order_note"].ToString(),
                        Payment_Id = (int)sdr["Payment_Id"],
                        Shop_Id = (int)sdr["Shop_Id"]
                    };
                    orders.Add(order);
                }
            }
            return orders;
        }



    }
}
