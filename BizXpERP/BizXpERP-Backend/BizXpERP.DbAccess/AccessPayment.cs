﻿using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BizXpERP.DbAccess
{
    public class AccessPayment
    {
        private string connectionString;

        public AccessPayment(string con)
        {
            connectionString = con;
        }

        /// <summary>
        /// Method for Inserting Payment in the DB
        /// </summary>
        /// <param name="Payment"></param>
        public void AddPayment(Payment payment)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdatePayment", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Payment_Amount", payment.Payment_Amount);
                cmd.Parameters.AddWithValue("@Total_discount", payment.Total_discount);
                cmd.Parameters.AddWithValue("@Actual_Payment", payment.Actual_Payment);
                cmd.Parameters.AddWithValue("@Payment_process", payment.Payment_process);
                cmd.Parameters.AddWithValue("@Payee_Address", payment.Payee_Address);
                cmd.Parameters.AddWithValue("@Payee_ContactNo", payment.Payee_ContactNo);
                cmd.Parameters.AddWithValue("@Due_Amount", payment.Due_Amount);
                cmd.Parameters.AddWithValue("@Shop_id", payment.Shop_id);
                cmd.Parameters.AddWithValue("@Created_date", payment.Created_date);
                cmd.Parameters.AddWithValue("@Is_delete", payment.Is_delete);
                cmd.Parameters.AddWithValue("@Is_settled", payment.Is_settled);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void UpdatePayment(Payment payment)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdatePayment", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Payment_Id", payment.Payment_Id);
                cmd.Parameters.AddWithValue("@Payment_Amount", payment.Payment_Amount);
                cmd.Parameters.AddWithValue("@Total_discount", payment.Total_discount);
                cmd.Parameters.AddWithValue("@Actual_Payment", payment.Actual_Payment);
                cmd.Parameters.AddWithValue("@Payment_process", payment.Payment_process);
                cmd.Parameters.AddWithValue("@Payee_Address", payment.Payee_Address);
                cmd.Parameters.AddWithValue("@Payee_ContactNo", payment.Payee_ContactNo);
                cmd.Parameters.AddWithValue("@Due_Amount", payment.Due_Amount);
                cmd.Parameters.AddWithValue("@Shop_id", payment.Shop_id);
                cmd.Parameters.AddWithValue("@Is_delete", payment.Is_delete);
                cmd.Parameters.AddWithValue("@Is_settled", payment.Is_settled);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public Payment GetPaymentsById(int? id)
        {
            Payment payment = new Payment();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetPaymentsById", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Payment_Id", id);
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    payment.Payment_Id = (int)sdr["Payment_Id"];
                    payment.Payment_Amount= (decimal)sdr["Payment_Amount"];
                    payment.Total_discount= (decimal)sdr["Total_discount"];
                    payment.Actual_Payment= (decimal)sdr["Actual_Payment"];
                    payment.Payment_process = sdr["Payment_process"].ToString();
                    payment.Payee_Address= sdr["Payee_Address"].ToString();
                    payment.Payee_ContactNo= sdr["Payee_ContactNo"].ToString();
                    payment.Due_Amount= (decimal)sdr["Due_Amount"];
                    payment.Shop_id= (int)sdr["Shop_id"];
                    payment.Created_date = (DateTime)sdr["Created_date"];
                    payment.Is_delete = (bool)sdr["Is_delete"];
                    payment.Is_settled = (bool)sdr["Is_settled"];
                }
            }
            return payment;
        }

        public IEnumerable<Payment> GetPaymentsByDueAmount(double dueAmount)
        {
            List<Payment> lstPayment = new List<Payment>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetPaymentsByDueAmount", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DueAmount", dueAmount);
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Payment payment = new Payment();
                    payment.Payment_Id = (int)sdr["Payment_Id"];
                    payment.Payment_Amount = (decimal)sdr["Payment_Amount"];
                    payment.Total_discount = (decimal)sdr["Total_discount"];
                    payment.Actual_Payment = (decimal)sdr["Actual_Payment"];
                    payment.Payment_process = sdr["Payment_process"].ToString();
                    payment.Payee_Address = sdr["Payee_Address"].ToString();
                    payment.Payee_ContactNo = sdr["Payee_ContactNo"].ToString();
                    payment.Due_Amount = (decimal)sdr["Due_Amount"];
                    payment.Shop_id = (int)sdr["Shop_id"];
                    payment.Created_date = (DateTime)sdr["Created_date"];
                    payment.Is_delete = (bool)sdr["Is_delete"];
                    payment.Is_settled = (bool)sdr["Is_settled"];

                    lstPayment.Add(payment);

                }
            }
            return lstPayment;
        }

        public IEnumerable<Payment> GetPaymentsByDate(DateTime? startDate,DateTime? endDate, int shopId)
        {
            List<Payment> lstPayment = new List<Payment>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetPaymentsByDate", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@StartDate", startDate);
                cmd.Parameters.AddWithValue("@EndDate", endDate);
                cmd.Parameters.AddWithValue("@Shop_Id", shopId);
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Payment payment = new Payment();
                    payment.Payment_Id = (int)sdr["Payment_Id"];
                    payment.Payment_Amount = (decimal)sdr["Payment_Amount"];
                    payment.Total_discount = (decimal)sdr["Total_discount"];
                    payment.Actual_Payment = (decimal)sdr["Actual_Payment"];
                    payment.Payment_process = sdr["Payment_process"].ToString();
                    payment.Payee_Address = sdr["Payee_Address"].ToString();
                    payment.Payee_ContactNo = sdr["Payee_ContactNo"].ToString();
                    payment.Due_Amount = (decimal)sdr["Due_Amount"];
                    payment.Shop_id = (int)sdr["Shop_id"];
                    payment.Created_date = (DateTime)sdr["Created_date"];
                    payment.Is_delete = (bool)sdr["Is_delete"];
                    payment.Is_settled = (bool)sdr["Is_settled"];

                    lstPayment.Add(payment);
                }
            }
            return lstPayment;
        }
    }
}
