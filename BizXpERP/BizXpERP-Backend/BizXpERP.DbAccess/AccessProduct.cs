﻿using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BizXpERP.DbAccess
{
    public class AccessProduct
    {
        private string connectionString;

        public AccessProduct(string con)
        {
            connectionString = con;
        }

        /// <summary>
        /// Method for Inserting Product in the DB
        /// </summary>
        /// <param name="Product"></param>
        public void AddProduct(Product product)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateProduct", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Product_Id", product.Product_Id);
                cmd.Parameters.AddWithValue("@Name", product.Name);
                cmd.Parameters.AddWithValue("@ProductCategory_Id", product.ProductCategory_Id);
                cmd.Parameters.AddWithValue("@Vendor_id", product.Vendor_id);
                cmd.Parameters.AddWithValue("@Shop_id", product.Shop_id);
                cmd.Parameters.AddWithValue("@Unit_price", product.Unit_price);
                cmd.Parameters.AddWithValue("@is_delete", product.is_delete);
                cmd.Parameters.AddWithValue("@Expire_date", product.Expire_date);
                cmd.Parameters.AddWithValue("@OverView", product.OverView);
                cmd.Parameters.AddWithValue("@User_Id", product.User_Id);
                cmd.Parameters.AddWithValue("@Img_url", "");

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public IEnumerable<Product> GetProductsFromShop(int? userId, int? shopId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetProductsFromShop", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@User_Id", userId);
                cmd.Parameters.AddWithValue("@Shop_Id", shopId);

                List<Product> products = new List<Product>();

                con.Open();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var product = new Product
                        {
                            Product_Id = (int)dr["Product_Id"],
                            Name = dr["Name"].ToString(),
                            ProductCategory_Id = (int)dr["ProductCategory_Id"],
                            Vendor_id = (int)dr["Vendor_id"],
                            Shop_id = (int)dr["Shop_id"],
                            Unit_price = (int)dr["Unit_price"],
                            Created_date = (DateTime)dr["Created_date"],
                            is_delete = (bool)dr["Is_delete"],
                            Expire_date = (DateTime)dr["Expire_date"],
                            OverView = dr["OverView"].ToString(),
                            User_Id = (int)dr["User_Id"],
                            Img_url = dr["Img_url"].ToString()
                        };

                        products.Add(product);
                    }
                }
                con.Close();
                return products;
            }
        }

        public void UpdateProduct(Product product)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateProduct", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Product_Id", product.Product_Id);
                cmd.Parameters.AddWithValue("@Name", product.Name);
                cmd.Parameters.AddWithValue("@ProductCategory_Id", product.ProductCategory_Id);
                cmd.Parameters.AddWithValue("@Vendor_id", product.Vendor_id);
                cmd.Parameters.AddWithValue("@Shop_id", product.Shop_id);
                cmd.Parameters.AddWithValue("Unit_price", product.Unit_price);
                cmd.Parameters.AddWithValue("@is_delete", product.is_delete);
                cmd.Parameters.AddWithValue("@Expire_date", product.Expire_date);
                cmd.Parameters.AddWithValue("@OverView", product.OverView);
                cmd.Parameters.AddWithValue("@User_Id", product.User_Id);
                cmd.Parameters.AddWithValue("@Img_url", product.Img_url);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public Product GetProductsById(int? id)
        {
            Product product = new Product();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetProductById", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Product_Id", id);
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    product.Product_Id = (int)sdr["Product_Id"];
                    product.Name = sdr["Name"].ToString();
                    product.ProductCategory_Id = (int)sdr["ProductCategory_Id"];
                    product.Vendor_id = (int)sdr["Vendor_id"];
                    product.Shop_id = (int)sdr["Shop_id"];
                    product.Unit_price = (decimal)sdr["Unit_price"];
                    product.Created_date = (DateTime)sdr["Created_date"];
                    product.is_delete = (bool)sdr["Is_delete"];
                    product.Expire_date = (DateTime)sdr["Expire_date"];
                    product.OverView = sdr["OverView"].ToString();
                    product.User_Id = (int)sdr["User_Id"];
                    product.Img_url = sdr["Img_url"].ToString();
                }
            }
            return product;
        }

        public IEnumerable<Product> GetProductsByCategoryId(int categoryId)
        {
            List<Product> lstProduct = new List<Product>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetProductCategoryById", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ProductCategory_Id", categoryId);
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Product product = new Product();
                    product.Product_Id = (int)sdr["Product_Id"];
                    product.Name = sdr["Name"].ToString();
                    product.ProductCategory_Id = (int)sdr["ProductCategory_Id"];
                    product.Vendor_id = (int)sdr["Vendor_id"];
                    product.Shop_id = (int)sdr["Shop_id"];
                    product.Unit_price = (int)sdr["Unit_price"];
                    product.Created_date = (DateTime)sdr["Created_date"];
                    product.is_delete = (bool)sdr["Is_delete"];
                    product.Expire_date = (DateTime)sdr["Expire_date"];
                    product.OverView = sdr["OverView"].ToString();
                    product.User_Id = (int)sdr["User_Id"];
                    product.Img_url = sdr["Img_url"].ToString();

                    lstProduct.Add(product);
                }
            }
            return lstProduct;
        }

        /// <summary>
        /// Get Product List accordingto the ShopId and the VendorId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Product> GetProductsByV_S_Id(int vendorId,int shopId)
        {
            List<Product> lstProduct = new List<Product>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetProductsByVendorIdAndShopId", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Vendor_Id", vendorId);
                cmd.Parameters.AddWithValue("@Shop_Id", shopId);
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    Product product = new Product
                    {
                        Product_Id = (int)sdr["Product_Id"],
                        Name = sdr["Name"].ToString(),
                        ProductCategory_Id = (int)sdr["ProductCategory_Id"],
                        Vendor_id = (int)sdr["Vendor_id"],
                        Shop_id = (int)sdr["Shop_id"],
                        Unit_price = (decimal)sdr["Unit_price"],
                        Created_date = (DateTime)sdr["Created_date"],
                        is_delete = (bool)sdr["Is_delete"],
                        Expire_date = (DateTime)sdr["Expire_date"],
                        OverView = sdr["OverView"].ToString(),
                        User_Id = (int)sdr["User_Id"],
                        Img_url = sdr["Img_url"].ToString()
                    };

                    lstProduct.Add(product);
                }
            }
            return lstProduct;
        }

        public IEnumerable<Product> GetProductsByDate_Count(DateTime StartDate,DateTime EndDate, int count)
        {
            List<Product> lstProduct = new List<Product>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetProductsByVendorIdAndShopId", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                cmd.Parameters.AddWithValue("@Count", count);
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Product product = new Product();
                    product.Product_Id = (int)sdr["Product_Id"];
                    product.Name = sdr["Name"].ToString();
                    product.ProductCategory_Id = (int)sdr["ProductCategory_Id"];
                    product.Vendor_id = (int)sdr["Vendor_id"];
                    product.Shop_id = (int)sdr["Shop_id"];
                    product.Unit_price = (int)sdr["Unit_price"];
                    product.Created_date = (DateTime)sdr["Created_date"];
                    product.is_delete = (bool)sdr["Is_delete"];
                    product.Expire_date = (DateTime)sdr["Expire_date"];
                    product.OverView = sdr["OverView"].ToString();
                    product.User_Id = (int)sdr["User_Id"];
                    product.Img_url = sdr["Img_url"].ToString();

                    lstProduct.Add(product);

                }
            }
            return lstProduct;
        }


    }
}
