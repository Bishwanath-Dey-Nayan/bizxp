﻿using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BizXpERP.DbAccess
{
    public class AccessProductCategory
    {
        private string connectionString;

        public AccessProductCategory(string conStr)
        {
            connectionString = conStr;
        }

        public void AddProductCategory(ProductCategory productCategory)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateProductaCategory", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ProductCategory_Id", productCategory.ProductCategory_Id);
                cmd.Parameters.AddWithValue("@Name", productCategory.Name);
                cmd.Parameters.AddWithValue("@User_Id", productCategory.User_id);
                cmd.Parameters.AddWithValue("@Is_delete", productCategory.Is_delete);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void UpdateProductCategory(ProductCategory productCategory)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateProductaCategory", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ProductCategory_Id", productCategory.ProductCategory_Id);
                cmd.Parameters.AddWithValue("@Name", productCategory.Name);
                cmd.Parameters.AddWithValue("@User_Id", productCategory.User_id);
                cmd.Parameters.AddWithValue("@Is_delete", productCategory.Is_delete);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
    }
}
