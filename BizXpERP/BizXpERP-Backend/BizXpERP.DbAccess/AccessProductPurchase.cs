﻿using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BizXpERP.DbAccess
{
    public class AccessProductPurchase
    {
        private string connectionString;

        public AccessProductPurchase(string conStr)
        {


            connectionString = conStr;
        }

        public void AddProuductPurchase(ProductPurchase pp)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateProductPurchase", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Purchase_Id", pp.Purchase_Id);
                cmd.Parameters.AddWithValue("@Product_Id", pp.Product_Id);
                cmd.Parameters.AddWithValue("@Quantity", pp.Quantity);
                cmd.Parameters.AddWithValue("@Total_price", pp.Total_price);
                cmd.Parameters.AddWithValue("@Unit_price", pp.Unit_price);
                cmd.Parameters.AddWithValue("@User_Id", pp.User_id);
                cmd.Parameters.AddWithValue("@Vendor_Id", pp.Vendor_id);
                cmd.Parameters.AddWithValue("@Shop_Id", pp.Shop_id);
                cmd.Parameters.AddWithValue("@Is_delete", pp.Is_delete);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }


        public void UpdateProductPurchase(ProductPurchase pp)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateProductPurchase", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Purchase_Id", pp.Purchase_Id);
                cmd.Parameters.AddWithValue("@Product_Id", pp.Product_Id);
                cmd.Parameters.AddWithValue("@Quantity", pp.Quantity);
                cmd.Parameters.AddWithValue("@Total_price", pp.Total_price);
                cmd.Parameters.AddWithValue("@Unit_price", pp.Unit_price);
                cmd.Parameters.AddWithValue("@User_Id", pp.User_id);
                cmd.Parameters.AddWithValue("@Vendor_Id", pp.Vendor_id);
                cmd.Parameters.AddWithValue("@Shop_Id", pp.Shop_id);
                cmd.Parameters.AddWithValue("@Is_delete", pp.Is_delete);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        /// <summary>
        /// Method to get the list of ProductPurchase by shopid and userId
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="shopId"></param>
        /// <returns></returns>
        public IEnumerable<ProductPurchase> GetProductPurchaseByUserIdandShopId(int UserId, int shopId)
        {
            List<ProductPurchase> lstpp = new List<ProductPurchase>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("GetProductPurchaseByUseIdAndShopId",con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Shop_id", shopId);
                cmd.Parameters.AddWithValue("@User_id", UserId);

                con.Open();

                SqlDataReader sdr = cmd.ExecuteReader();

                while(sdr.Read())
                {
                    ProductPurchase pp = new ProductPurchase();

                    pp.Purchase_Id = (int)sdr["Purchase_Id"];
                    pp.Quantity = (int)sdr["Quantity"];
                    pp.Total_price = (Decimal)sdr["Total_price"];
                    pp.Unit_price = (Decimal)sdr["Unit_price"];
                    pp.User_id = (int)sdr["User_id"];
                    pp.Vendor_id = (int)sdr["Vendor_id"];
                    pp.Shop_id = (int)sdr["Shop_id"];
                    pp.Created_date = (DateTime)sdr["Created_date"];
                    pp.Is_delete = (bool)sdr["Is_delete"];

                    lstpp.Add(pp);
                }

            }

            return lstpp;
        }
    }
}
