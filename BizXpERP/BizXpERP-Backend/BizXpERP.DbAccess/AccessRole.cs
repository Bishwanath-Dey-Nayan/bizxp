﻿using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BizXpERP.DbAccess
{
    public class AccessRole
    {
        private string connectionString;

        public AccessRole(string conStr)
        {
            connectionString = conStr;
        }

        public void AddRole(Role role)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateRole", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Role_Id", role.Role_Id);
                cmd.Parameters.AddWithValue("@Name", role.Name);
                cmd.Parameters.AddWithValue("@Created_date", role.Created_date);
                cmd.Parameters.AddWithValue("@Employer_Id", role.Employer_Id);
                cmd.Parameters.AddWithValue("@Shop_Id", role.Shop_Id);
                cmd.Parameters.AddWithValue("@Is_Delete", role.Is_Delete);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void UpdateRole(Role role)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateRole", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Role_Id", role.Role_Id);
                cmd.Parameters.AddWithValue("@Name", role.Name);
                cmd.Parameters.AddWithValue("@Created_date", role.Created_date);
                cmd.Parameters.AddWithValue("@Employer_Id", role.Employer_Id);
                cmd.Parameters.AddWithValue("@Shop_Id", role.Shop_Id);
                cmd.Parameters.AddWithValue("@Is_Delete", role.Is_Delete);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
    }
}
