﻿using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BizXpERP.DbAccess
{
    public class AccessRolePermission
    {
        private string connectionString;

        public AccessRolePermission(string conStr)
        {
            conStr = connectionString;
        }

        public void AddRolePermission(RolePermission rp)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateRolePermission", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@RolePermission_Id", rp.RolePermission_Id);
                cmd.Parameters.AddWithValue("@Name", rp.Name);
                cmd.Parameters.AddWithValue("@Role_Id", rp.Role_Id);
                cmd.Parameters.AddWithValue("@Is_Delete", rp.Is_Delete);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void UpdateRolePermission(RolePermission rp)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateRolePermission", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@RolePermission_Id", rp.RolePermission_Id);
                cmd.Parameters.AddWithValue("@Name", rp.Name);
                cmd.Parameters.AddWithValue("@Role_Id", rp.Role_Id);
                cmd.Parameters.AddWithValue("@Is_Delete", rp.Is_Delete);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
    }
}
