﻿using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BizXpERP.DbAccess
{
    public class AccessShopDetails
    {
        private string connectionString;

        public AccessShopDetails(string conStr)
        {
            connectionString = conStr;
        }

        public ShopDetails GetShopDetails(int? ownerId)
        {
            ShopDetails shop = new ShopDetails();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * from Shop_Details where Owner_Id=@Owner_Id", con);
                cmd.Parameters.AddWithValue("@Owner_Id", ownerId);
                con.Open();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    shop.Shop_Id = Convert.ToInt32(sdr["Shop_Id"]);
                    shop.Name = sdr["Name"].ToString();
                    shop.Address = sdr["Address"].ToString();
                    shop.Area = sdr["Area"].ToString();
                    shop.City = sdr["City"].ToString();
                    shop.Owner_Id = (int)sdr["Owner_Id"];
                    shop.Created_Date = (DateTime)sdr["Created_date"];
                    shop.Is_delete = (bool)sdr["Is_delete"];
                    shop.Shop_Id = (int)sdr["Shop_Id"];
                }
            }
            return shop;
        }
        public void AddShopDetails(ShopDetails sd)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateShopDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Shop_Id", sd.Shop_Id);
                cmd.Parameters.AddWithValue("@Name", sd.Name);
                cmd.Parameters.AddWithValue("@Address", sd.Address);
                cmd.Parameters.AddWithValue("@Area", sd.Area);
                cmd.Parameters.AddWithValue("@City", sd.City);
                cmd.Parameters.AddWithValue("@Owner_Id", sd.Owner_Id);
                cmd.Parameters.AddWithValue("@Is_delete", sd.Is_delete);
                cmd.Parameters.AddWithValue("@Img_url", sd.Img_url);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void UpdateShopDetails(ShopDetails sd)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateShopDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Shop_Id", sd.Shop_Id);
                cmd.Parameters.AddWithValue("@Name", sd.Name);
                cmd.Parameters.AddWithValue("@Address", sd.Address);
                cmd.Parameters.AddWithValue("@Area", sd.Area);
                cmd.Parameters.AddWithValue("@City", sd.City);
                cmd.Parameters.AddWithValue("@Owner_Id", sd.Owner_Id);
                cmd.Parameters.AddWithValue("@Is_delete", sd.Is_delete);
                cmd.Parameters.AddWithValue("@Img_url", sd.Img_url);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
    }
}
