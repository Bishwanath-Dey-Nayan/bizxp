﻿using BizXpERP.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BizXpERP.DbAccess
{
    public class AccessUserDetails
    {
        private readonly string connectionString;

        public AccessUserDetails(string conStr)
        {
            connectionString = conStr;
        }

        public UserDetails GetUser(UserDetails user)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                UserDetails returnedUser = new UserDetails();
                SqlCommand cmd = new SqlCommand("LoginUser", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Email", user.Email);
                cmd.Parameters.AddWithValue("@Pass", user.Pass);
                cmd.Parameters.AddWithValue("@Mobile", 01);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if(dr.HasRows)
                    returnedUser.Name = dr["Name"].ToString();
                    returnedUser.User_Id = Convert.ToInt32(dr["UserId"]);
                }
                return returnedUser;
            }
        }

        public void AddUser(UserDetails userDetails)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateUserDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@User_Id", userDetails.User_Id);
                cmd.Parameters.AddWithValue("@Name", userDetails.Name);
                cmd.Parameters.AddWithValue("@Pass", userDetails.Pass);
                cmd.Parameters.AddWithValue("@Role_id", userDetails.Role_id);
                cmd.Parameters.AddWithValue("@Employer_Id", userDetails.Employer_Id);
                cmd.Parameters.AddWithValue("@Mobile", userDetails.Mobile);
                cmd.Parameters.AddWithValue("@Email", userDetails.Email);
                cmd.Parameters.AddWithValue("@Is_Active", userDetails.Is_Active);
                cmd.Parameters.AddWithValue("@Img_url", userDetails.Img_url);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }


        public void UpdateUserDetails(UserDetails userDetails)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateUserDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@User_Id", userDetails.User_Id);
                cmd.Parameters.AddWithValue("@Name", userDetails.Name);
                cmd.Parameters.AddWithValue("@Pass", userDetails.Pass);
                cmd.Parameters.AddWithValue("@Role_id", userDetails.Role_id);
                cmd.Parameters.AddWithValue("@Employer_Id", userDetails.Employer_Id);
                cmd.Parameters.AddWithValue("@Mobile", userDetails.Mobile);
                cmd.Parameters.AddWithValue("@Email", userDetails.Email);
                cmd.Parameters.AddWithValue("@Is_Active", userDetails.Is_Active);
                cmd.Parameters.AddWithValue("@Img_url", userDetails.Img_url);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

    }
}
