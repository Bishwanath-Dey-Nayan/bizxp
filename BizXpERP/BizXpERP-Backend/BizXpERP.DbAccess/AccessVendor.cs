﻿using BizXpERP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BizXpERP.DbAccess
{
    public class AccessVendor
    {
        private string connectionString;

        public AccessVendor(string con)
        {
            connectionString = con;
        }

        /// <summary>
        /// Method for Inserting Vendor in the DB
        /// </summary>
        /// <param name="vendor"></param>
        public void AddVendor(Vendor vendor)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateVendor", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Vendor_Id", vendor.Vendor_Id);
                cmd.Parameters.AddWithValue("@Name", vendor.Name);
                cmd.Parameters.AddWithValue("@Location", vendor.Location);
                cmd.Parameters.AddWithValue("@Phone_Number", vendor.Phone_Number);
                cmd.Parameters.AddWithValue("@Is_delete", vendor.Is_delete);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void UpdateVendor(Vendor vendor)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("InsertUpdateVendor", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Vendor_Id", vendor.Vendor_Id);
                cmd.Parameters.AddWithValue("@Name", vendor.Name);
                cmd.Parameters.AddWithValue("@Location", vendor.Location);
                cmd.Parameters.AddWithValue("@Phone_Number", vendor.Phone_Number);
                cmd.Parameters.AddWithValue("@Is_delete", vendor.Is_delete);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
    }
}
