﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.Models
{
    public class Dashboard
    {
        public int Total_Sales { get; set; }
        public string Total_Profit { get; set; }
        public string Total_Due { get; set; }
    }
}
