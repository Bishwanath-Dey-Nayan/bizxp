﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.Models
{
    class LoginStatus
    {
        public int LoginStatus_Id { get; set; }
        public int User_Id { get; set; }
        public int Shop_Id { get; set; }
        public DateTime Last_login_time { get; set; }
    }
}
