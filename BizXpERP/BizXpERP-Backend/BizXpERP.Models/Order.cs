﻿using System;

namespace BizXpERP.Models
{
    public class Order
    {
        public int Order_Id { get; set; }
        public int Product_Id { get; set; }
        public int Quantity { get; set; }
        public decimal Discount { get; set; }
        public decimal Total_price { get; set; }
        public decimal Actual_price { get; set; }
        public int User_Id { get; set; }
        public DateTime Created_date { get; set; }
        public bool Is_delete { get; set; }
        public string Order_note { get; set; }
        public int Payment_Id { get; set; }
        public int Shop_Id { get; set; }
    }
}
