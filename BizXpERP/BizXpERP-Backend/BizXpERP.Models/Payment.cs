﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.Models
{
    public class Payment
    {
        public int Payment_Id { get; set; }
        public decimal Payment_Amount { get; set; }
        public decimal Total_discount { get; set; }
        public decimal Actual_Payment { get; set; }
        public string Payment_process { get; set; }
        public string Payee_Address { get; set; }
        public string Payee_ContactNo { get; set; }
        public decimal Due_Amount { get; set; }
        public int Shop_id { get; set; }
        public DateTime Created_date { get; set; }
        public bool Is_delete { get; set; }
        public bool Is_settled { get; set; }
    }
}
