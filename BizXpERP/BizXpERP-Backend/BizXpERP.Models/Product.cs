﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.Models
{
     public class Product
    {
        public int Product_Id { get; set; }
        public string Name { get; set; }
        public int ProductCategory_Id { get; set; }
        public int Vendor_id { get; set; }
        public int Shop_id { get; set; }
        public decimal Unit_price { get; set; }
        public DateTime Created_date { get; set; }
        public bool is_delete { get; set; }
        public DateTime Expire_date { get; set; }
        public string OverView { get; set; }
        public int User_Id { get; set; }
        public string Img_url { get; set; }
    }
}
