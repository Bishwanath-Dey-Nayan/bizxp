﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.Models
{
    public class ProductCategory
    {
        public int ProductCategory_Id { get; set; }
        public string Name { get; set; }
        public int User_id { get; set; }
        public DateTime Created_date { get; set; }
        public bool Is_delete { get; set; }
    }
}
