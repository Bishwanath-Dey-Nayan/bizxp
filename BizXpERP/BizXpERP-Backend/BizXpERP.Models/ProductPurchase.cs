﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.Models
{
    public class ProductPurchase
    {
        public int Purchase_Id { get; set; }
        public int Quantity { get; set; }
        public decimal Total_price { get; set; }
        public decimal Unit_price { get; set; }
        public int User_id { get; set; }
        public int Vendor_id { get; set; }
        public int Shop_id { get; set; }
        public DateTime Created_date { get; set; }
        public bool Is_delete { get; set; }
        public int Product_Id { get; set; }
    }
}
