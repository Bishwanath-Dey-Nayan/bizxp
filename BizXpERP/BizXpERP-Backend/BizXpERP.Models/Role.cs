﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.Models
{
    public class Role
    {
        public int Role_Id { get; set; }
        public string Name { get; set; }
        public DateTime Created_date { get; set;}
        public int Employer_Id { get; set; }
        public int Shop_Id { get; set; }
        public bool Is_Delete { get; set; }


    }
}
