﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.Models
{
    public class RolePermission
    {
        public int RolePermission_Id { get; set; }
        public string Name { get; set; }
        public int Role_Id { get; set; }
        public DateTime Created_date { get; set; }
        public bool Is_Delete { get; set; }
    }
}
