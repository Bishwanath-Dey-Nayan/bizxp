﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.Models
{
    public class ShopDetails
    {
        public int Shop_Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public int Owner_Id { get; set; }
        public DateTime Created_Date { get; set; }
        public bool Is_delete { get; set; }
        public string Img_url { get; set; }
    }
}
