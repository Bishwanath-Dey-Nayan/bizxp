﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.Models
{
    public class UserDetails
    {
        public int User_Id { get; set; }
        public string Name { get; set; }
        public string Pass { get; set; }
        public int Role_id { get; set; }
        public int Employer_Id { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public bool Is_Active { get; set; }
        public DateTime Created_date { get; set; }
        public string Img_url { get; set; }
    }
}
