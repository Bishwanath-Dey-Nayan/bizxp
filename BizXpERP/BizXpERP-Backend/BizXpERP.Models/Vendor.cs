﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BizXpERP.Models
{
     public class Vendor
    {
        public int Vendor_Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Phone_Number { get; set; }
        public DateTime Created_date { get; set; }
        public bool Is_delete { get; set; }
    }
}
