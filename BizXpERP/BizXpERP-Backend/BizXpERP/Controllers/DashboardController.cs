﻿using BizXpERP.BusinessLayer;
using BizXpERP.Models;
using Microsoft.AspNetCore.Mvc;
using System;

namespace BizXpERP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly DashboardRepository dr;

        public DashboardController()
        {
            dr = new DashboardRepository(Startup.ConnectionString);
        }

        [HttpGet]
        [Route("GetDashboard")]
        public ActionResult<Dashboard> GetDashboard(int shopid, string startdate, string enddate)
        {
            DateTime StartDate, EndDate;
            if (shopid != 0 && DateTime.TryParse(startdate, out StartDate) && DateTime.TryParse(enddate, out EndDate))
            {
                return Ok(dr.GetDashboard(shopid, StartDate, EndDate));
            }
            return BadRequest("Data was not found");
        }
    }
}