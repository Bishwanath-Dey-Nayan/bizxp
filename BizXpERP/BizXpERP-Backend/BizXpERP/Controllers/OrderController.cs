﻿using BizXpERP.BusinessLayer;
using BizXpERP.Models;
using Microsoft.AspNetCore.Mvc;

namespace BizXpERP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly OrderRepository or;

        public OrderController()
        {
            or = new OrderRepository(Startup.ConnectionString);
        }

        [HttpPost]
        [Route("CreateOrder")]
        public ActionResult CreateOrder([FromBody] Order order)
        {
            if (order != null)
            {
                or.AddOrder(order);
                return StatusCode(201, "Data has been Saved Successfully");
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("GetOrder")]
        public ActionResult<string> GetOrder()
        {
            return "called get order";
        }

        [HttpPut]
        [Route("UpdateOrder")]
        public ActionResult UpdateOrder([FromBody] Order order)
        {
            if (order != null)
            {
                or.UpdateOrder(order);
                return StatusCode(200, "Updated Successfully");
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("GetOrdersById")]
        public ActionResult<Order> GetOrdersById(int? id)
        {
            if (id != null)
            {
                var orderData = or.GetOrdersById(id);
                return Ok(orderData);
            }
            return BadRequest();
        }

        /// <summary>
        /// Method to get the payment details according to the paymentId
        /// </summary>
        /// <param name="paymentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetOrdersByPaymentId")]
        public ActionResult<Order> GetOrdersByPaymentId(int? paymentId)
        {
            if (paymentId != null)
            {
                var orderData = or.GetOrdersByPaymentId(paymentId);
                return Ok(orderData);
            }
            return BadRequest();
        }
    }
}