﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BizXpERP.BusinessLayer;
using BizXpERP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BizXpERP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly PaymentRepository pr;

        public PaymentController()
        {
            pr = new PaymentRepository(Startup.ConnectionString);
        }

        [HttpPost]
        [Route("CreatePayment")]
        public ActionResult CreatePayment([FromBody] Payment payment)
        {
            if (payment != null)
            {
                pr.AddPayment(payment);
                return StatusCode(201, "Data has been Saved Successfully");
            }
            return BadRequest();
        }

        [HttpPut]
        [Route("UpdatePayment")]
        public ActionResult UpdatePayment([FromBody] Payment payment)
        {
            if (payment != null)
            {
                pr.UpdatePayment(payment);
                return StatusCode(200, "Updatd Successfully");
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("GetPaymentById")]
        public ActionResult<Payment> GetPaymentById(int? id)
        {
            if (id != null)
            {
                var paymentData = pr.GetPaymentsById(id);
                return Ok(paymentData);
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("GetPaymentsByDueAmount")]

        public ActionResult<List<Payment>> GetPaymentsByDueAmount(double dueAmount)
        {
            if(dueAmount != null)
            {
                var paymentData = pr.GetPaymentsByDueAmount(dueAmount);
                return Ok(paymentData);
            }
            return BadRequest();

        }

        [HttpGet]
        [Route("GetPaymentsByDate")]

        public ActionResult<List<Payment>> GetPaymentsByDate(DateTime startDate, DateTime endDate, int shopId)
        {
            if (startDate != null && endDate!= null && shopId!=null)
            {
                var paymentData = pr.GetPaymentsByDate(startDate,endDate,shopId);
                return Ok(paymentData);
            }
            return BadRequest();

        }
    }
}