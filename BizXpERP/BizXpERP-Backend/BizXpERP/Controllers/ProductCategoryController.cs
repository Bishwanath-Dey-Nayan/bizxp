﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BizXpERP.DbAccess;
using BizXpERP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BizXpERP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductCategoryController : ControllerBase
    {
        private readonly AccessProductCategory apc;

        public ProductCategoryController()
        {
            apc = new AccessProductCategory(Startup.ConnectionString);
        }

        [HttpPost]
        [Route("CreateProductCategory")]
        public ActionResult CreateProductCategory([FromBody] ProductCategory pc)
        {
            if (pc != null)
            {
                apc.AddProductCategory(pc);
                return StatusCode(201, "Data has been Saved Successfully");
            }
            return BadRequest();
        }

        [HttpPut]
        [Route("UpdateProductCategory")]
        public ActionResult UpdateProductCategory([FromBody] ProductCategory pc)
        {
            if (pc != null)
            {
                apc.UpdateProductCategory(pc);
                return StatusCode(200, "Updated Successfully");
            }
            return BadRequest();
        }


    }
}