﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using BizXpERP.BusinessLayer;
using BizXpERP.Models;

namespace BizXpERP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ProductRepository pr;

        public ProductController()
        {
            pr = new ProductRepository(Startup.ConnectionString);
        }

        [HttpPost]
        [Route("CreateProduct")]
        public ActionResult CreateProduct([FromBody] Product product)
        {
            if(product != null)
            {
                pr.AddProduct(product);
                return StatusCode(201, "Data has been Saved Successfully");
            }
            return BadRequest();
        }

        [HttpPut]
        [Route("UpdateProduct")]
        public ActionResult UpdateProduct([FromBody] Product product)
        {
            if(product != null)
            {
                pr.UpdateProduct(product);
                return StatusCode(200, "Updatd Successfully");
            }
            return BadRequest();
        }


        [HttpGet]
        [Route("GetProductById")]
        public ActionResult<Product> GetProductById(int? id)
        {
            if(id !=null)
            {
                var productData = pr.GetProductById(id);
                return Ok(productData);
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("GetProductsByCategoryId")]

        public ActionResult<List<Product>> GetProductsByCategoryId(int categoryId)
        {
            if (categoryId!= null)
            {
                var ProductData = pr.GetProductsByCategoryId(categoryId);
                return Ok(ProductData);
            }
            return BadRequest();

        }

        [HttpGet]
        [Route("GetProductsByV_S_Id")]

        public ActionResult<List<Product>> GetProductsByV_S_Id(int vendorId, int shopId)
        {
            if (vendorId != null && shopId != null)
            {
                var ProductData = pr.GetProductsByV_S_Id(vendorId,shopId);
                return Ok(ProductData);
            }
            return BadRequest();

        }

        [HttpGet]
        [Route("GetProductsByDate_Count")]

        public ActionResult<List<Product>> GetProductsByDate_Count(DateTime StartDate, DateTime EndDate, int count)
        {
            if (StartDate != null && EndDate != null && count != null)
            {
                var ProductData = pr.GetProductsByDate_Count(StartDate,EndDate,count);
                return Ok(ProductData);
            }
            return BadRequest();

        }

        //[HttpGet]
        //[Route("GetProductsFromShop")]
        //public ActionResult<List<Product>> GetProductsFromShop(int? UserId, int? ShopId)
        //{
        //    if(UserId != null && UserId != 0 && ShopId != null && ShopId != 0)
        //    {
        //        var ProductData = pr.GetProductsFromShop(UserId, ShopId);
        //        return Ok(ProductData);
        //    }
        //    return BadRequest();
        //}
    }
}