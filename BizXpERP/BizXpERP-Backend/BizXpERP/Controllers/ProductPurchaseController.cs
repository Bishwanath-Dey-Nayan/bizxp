﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BizXpERP.BusinessLayer;
using BizXpERP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BizXpERP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductPurchaseController : ControllerBase
    {
        private readonly ProductPurchaseRepository ppr;

        public ProductPurchaseController()
        {
            ppr = new ProductPurchaseRepository(Startup.ConnectionString);
        }

        [HttpGet]
        [Route("GetProductPurchaseByUserIdandShopId")]

        public ActionResult<List<ProductPurchase>> GetProductPurchaseByUserIdandShopId(int userId,int shopId)
        {
            var productPurchaseData = ppr.GetProductPurchaseByUserIdandShopId(userId, shopId);
            return Ok(productPurchaseData);
        }

        [HttpPost]
        [Route("CreateProductPurchase")]

        public ActionResult CreateProductPurchase([FromBody] ProductPurchase pp)
        {
            if(pp != null)
            {
                ppr.AddProductPurchase(pp);
                return StatusCode(201, "Data inserted successfully.");
            }
            return BadRequest();
        }

        [HttpPut]
        [Route("UpdateProductPurchase")]

        public ActionResult UpdateProductPurchase([FromBody] ProductPurchase pp)
        {
            if(pp != null)
            {
                ppr.UpdateProductPurchase(pp);
                return StatusCode(200, "Data updated successfully.");
            }
            return BadRequest();
        }


    }
}