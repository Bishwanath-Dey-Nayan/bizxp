﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BizXpERP.BusinessLayer;
using BizXpERP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BizXpERP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly RoleRepository rr;

        public RoleController()
        {
            rr = new RoleRepository(Startup.ConnectionString);
        }

        [HttpPost]
        [Route("CreateRole")]
        public ActionResult CreateRole([FromBody] Role role)
        {
            if (role != null)
            {
                rr.AddRole(role);
                return StatusCode(201, "Data has been Saved Successfully");
            }
            return BadRequest();
        }

        [HttpPut]
        [Route("UpdateRole")]
        public ActionResult UpdateRole([FromBody] Role role)
        {
            if (role != null)
            {
                rr.UpdateRole(role);
                return StatusCode(200, "Updated Successfully");
            }
            return BadRequest();
        }
    }
}