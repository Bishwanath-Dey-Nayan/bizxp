﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BizXpERP.BusinessLayer;
using BizXpERP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BizXpERP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolePermissionController : ControllerBase
    {
        private readonly RolePermissionRepository rpr;

        public RolePermissionController()
        {
            rpr = new RolePermissionRepository(Startup.ConnectionString);
        }

        [HttpPost]
        [Route("CreateRolePermission")]
        public ActionResult CreateRolePermission([FromBody] RolePermission rp)
        {
            if (rp != null)
            {
                rpr.AddRolePermisson(rp);
                return StatusCode(201, "Data has been Saved Successfully");
            }
            return BadRequest();
        }

        [HttpPut]
        [Route("UpdateRolePermission")]
        public ActionResult UpdateRolePermission([FromBody] RolePermission rp)
        {
            if (rp != null)
            {
                rpr.UpdateRolePermission(rp);
                return StatusCode(200, "Updated Successfully");
            }
            return BadRequest();
        }


    }
}