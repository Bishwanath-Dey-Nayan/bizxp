﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BizXpERP.BusinessLayer;
using BizXpERP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BizXpERP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShopDetailController : ControllerBase
    {

        private readonly ShopDetailRepository sdr;

        public ShopDetailController()
        {
            sdr = new ShopDetailRepository(Startup.ConnectionString);
        }

        [HttpGet]
        [Route("GetShopDetails")]
        public ActionResult<ShopDetails> GetShopDetails(int? owner_Id)
        {
            if (owner_Id != null)
            {
                var shopData = sdr.GetShopDetails(owner_Id);
                return Ok(shopData);
            }
            return BadRequest();
        }

        [HttpPost]
        [Route("CreateShopDetail")]
        public ActionResult CreateShopDetail([FromBody] ShopDetails sd)
        {
            if (sd != null)
            {
                sdr.AddShopDetail(sd);
                return StatusCode(201, "Data has been Saved Successfully");
            }
            return BadRequest();
        }

        [HttpPut]
        [Route("UpdateShopDetail")]
        public ActionResult UpdateShopDetail([FromBody] ShopDetails sd)
        {
            if (sd != null)
            {
                sdr.UpdateShopDetail(sd);
                return StatusCode(200, "Updated Successfully");
            }
            return BadRequest();
        }
    }
}