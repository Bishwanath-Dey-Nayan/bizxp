﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BizXpERP.BusinessLayer;
using BizXpERP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BizXpERP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserDetailsController : ControllerBase
    {
        private readonly UserDetailsRepository udr;

        public UserDetailsController()
        {
            udr = new UserDetailsRepository(Startup.ConnectionString);
        }


        [HttpGet]
        [Route("GetUser")]
        public ActionResult<UserDetails> GetUser(string email, string pass)
        {
            UserDetails user = new UserDetails() { Email = email, Pass = pass, Mobile = "021" };
            if (user != null)
            {
                var retrievedUser = udr.ValidateLogin(user);
                if (retrievedUser != null && retrievedUser.User_Id != 0)
                {
                    return Ok(retrievedUser);
                }
                else
                {
                    return null;
                }
            }
            return BadRequest();
        }
        [HttpPost]
        [Route("CreateUser")]
        public ActionResult CreateUser([FromBody] UserDetails ud)
        {
            if (ud != null)
            {
                udr.AddUser(ud);
                return StatusCode(201, "Data has been Saved Successfully");
            }
            return BadRequest();
        }

        [HttpPut]
        [Route("UpdateUserDetails")]
        public ActionResult UpdateUserDetails([FromBody] UserDetails ud)
        {
            if (ud != null)
            {
                udr.UpdateUserDetails(ud);
                return StatusCode(200, "Updated Successfully");
            }
            return BadRequest();
        }
    }
}