﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BizXpERP.BusinessLayer;
using BizXpERP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BizXpERP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendorController : ControllerBase
    {
        private readonly VendorRepository vr;

        public VendorController()
        {
            vr = new VendorRepository(Startup.ConnectionString);
        }

        [HttpPost]
        [Route("CreateVendor")]
        public ActionResult CreateVendor([FromBody] Vendor vendor)
        {
            if(vendor != null)
            {
                vr.AddVendor(vendor);
                return StatusCode(201, "Data has been saved successfully");
            }
            return BadRequest();
        }

        [HttpPut]
        [Route("UpdateVendor")]
        public ActionResult UpdateVendor([FromBody ] Vendor vendor)
        {
            if(vendor != null)
            {
                vr.UpdateVendor(vendor);
                return StatusCode(200, "Updated Successfully");
            }
            return BadRequest();
        }
    }
}